package randstring

import (
crypto "crypto/rand"
"encoding/base64"
	"math/rand"
)

var(
	cryptoGen = CryptoRndGen{}
	randGen = RandGen{}
)

func GenerateRandomStringSecure(len int) string {
	return GenerateRandomStr(len, cryptoGen)
}

func GenerateRandomString(len int) string {
	return GenerateRandomStr(len, randGen)
}

func GenerateRandomStr(len int, genBytes GenBytes) string {
	bytes := make([]byte, len)
	_, err := crypto.Read(bytes)
	if err != nil {
		panic(err)
	}
	return base64.StdEncoding.EncodeToString(bytes)
}

type GenBytes interface {
	GenRndBytes(bytes []byte) []byte
}

type CryptoRndGen struct {}
func (CryptoRndGen)GenRndBytes(bytes []byte) []byte{
	_, err := crypto.Read(bytes)
	if err != nil {
		panic(err)
	}
	return bytes
}

type RandGen struct {}
func (RandGen)GenRndBytes(bytes []byte) []byte{
	_, err := rand.Read(bytes)
	if err != nil {
		panic(err)
	}
	return bytes
}