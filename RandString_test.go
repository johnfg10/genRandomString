package randstring

import (
	"testing"
)

func TestGenerateRandomStringSecure(t *testing.T) {
	var t1, t2, t3, t4, t5 string
	t1 = GenerateRandomStringSecure(10)
	t2 = GenerateRandomStringSecure(10)
	t3 = GenerateRandomStringSecure(10)
	t4 = GenerateRandomStringSecure(10)
	t5 = GenerateRandomStringSecure(10)
	t.Logf("val 1: %v val 2: %v val 3: %v val 4: %v val 5: %v", t1, t2, t3, t4, t5)
	if allEquals(t1, t2, t3, t4, t5) {
		t.Fail()
	}
}

func TestGenerateRandomString(t *testing.T) {
	var t1, t2, t3, t4, t5 string
	t1 = GenerateRandomString(10)
	t2 = GenerateRandomString(10)
	t3 = GenerateRandomString(10)
	t4 = GenerateRandomString(10)
	t5 = GenerateRandomString(10)
	t.Logf("val 1: %v val 2: %v val 3: %v val 4: %v val 5: %v", t1, t2, t3, t4, t5)
	if allEquals(t1, t2, t3, t4, t5) {
		t.Fail()
	}
}
func allEquals(v ...interface{}) bool {
	if len(v) > 1 {
		a := v[0]
		for _, s := range v {
			if a != s {
				return false
			}
		}
	}
	return true
}